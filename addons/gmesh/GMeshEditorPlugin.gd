tool
extends EditorPlugin

var gmeshViz = preload("res://addons/gmesh/GMeshViz.gd").new()
var gmeshEdit = preload("res://addons/gmesh/GMeshEdit.gd").new()
var gmeshMenu = preload("res://addons/gmesh/GMeshMenu.tscn").instance()
var gmeshGizmo = preload("res://addons/gmesh/gizmo.gd").new()
var selected:Array

onready var interface:EditorInterface = get_editor_interface()
onready var selection:EditorSelection = interface.get_selection()

var is_active : = false
#var visible : = false

func get_plugin_name():
	return "GMeshEditor"

func _ready():

	gmeshViz.plugin = self
	gmeshEdit.plugin = self

	add_child(gmeshViz)
	add_child(gmeshEdit)

	selection.connect("selection_changed",self,"selection_changed")
	connect("scene_closed",self,"scene_closed")

	var popup = gmeshMenu.get_node("MenuButton").get_popup()
	popup.connect("index_pressed",self,"_gmeshMenu_index_pressed")
	popup.connect("about_to_show",self,"_gmeshMenu_about_to_show")
	var editCheckBox = gmeshMenu.get_node("EditCheckBox")
	editCheckBox.connect("toggled",self,"_gmeshMenu_edit_toggled")

	add_control_to_container(EditorPlugin.CONTAINER_SPATIAL_EDITOR_MENU,gmeshMenu)
	gmeshMenu.hide()

	add_spatial_gizmo_plugin(gmeshGizmo)

	pass

#func get_color(name:String, type:String = ""):
#	return get_editor_interface().get_base_control().get_color(name,type)

func _enter_tree():
	#interface = get_editor_interface()
	pass

func _exit_tree():
	clean()
	remove_control_from_container(EditorPlugin.CONTAINER_SPATIAL_EDITOR_MENU,gmeshMenu)
	remove_spatial_gizmo_plugin(gmeshGizmo)
	pass
func clean():
	is_active = false
	disable_editing()
	selected = Array()

func handles(object):
	is_active = false
	if object.is_class("MultiNodeEdit"):
		for node in selection.get_transformable_selected_nodes():
			if not node is MeshInstance:
				return false

	return object.is_class("MeshInstance") or object.is_class("MultiNodeEdit")

func edit(object):
	is_active = true

func disable_editing():
	for mesh_instance in selected:
		if gmeshViz.has_data(mesh_instance):
			gmeshViz.set_visible(mesh_instance,gmeshViz.VIZ_VISIBLE.NONE)
		if gmeshEdit.has_data(mesh_instance):
			gmeshEdit.set_editing(mesh_instance,false)

func clear():
	pass

func apply_changes():
	# apply changes to editing mesh here
	pass

func get_state():

	var state = {
		editState = gmeshEdit.get_state(),
		vizState = gmeshViz.get_state()
	}
	print("get_state: ",state)
	return state

func set_state(state):
	print("set_state: ",state)
	gmeshEdit.set_state(state.get("editState",{}))
	gmeshViz.set_state(state.get("vizState",{}))

func make_visible(visible):
	if visible:
		gmeshMenu.show()
	else:
		gmeshMenu.hide()

func _process(delta):
	for mesh_instance in selected:
		if gmeshViz.has_data(mesh_instance):
			gmeshViz.update_viz_instance(mesh_instance)

func forward_spatial_gui_input(camera:Camera, event:InputEvent):
	return gmeshEdit.handle_input(camera,event)

func forward_canvas_draw_over_viewport(overlay):
	gmeshViz.draw_over_viewport(overlay)
	gmeshEdit.draw_over_viewport(overlay)

func selection_changed():
	if is_active:
		var possible_selection = selection.get_transformable_selected_nodes()
		var new_selected = Array()

		# Keep any nodes that are still selected or editing, but disable the rest
		for mesh_instance in selected:
			if possible_selection.has(mesh_instance):
				new_selected.append(mesh_instance)
				print(hash(mesh_instance))
				possible_selection.erase(mesh_instance)
			else:
				if gmeshViz.has_data(mesh_instance):
					gmeshViz.set_visible(mesh_instance,gmeshViz.VIZ_VISIBLE.NONE)
#				if gmeshEdit.has_data(mesh_instance):
#					gmeshEdit.set_editing(mesh_instance,false)

		for node in possible_selection:
			if node is MeshInstance:
				new_selected.append(node)
				print(hash(node))
				if gmeshEdit.is_editing(node):
					var viz_data = gmeshViz.get_data(node)
					if viz_data.has_meta("mode") and viz_data.get_meta("mode") != null:
						gmeshViz.set_visible(node,viz_data.get_meta("mode"))
					else:
						gmeshViz.set_visible(node,gmeshViz.VIZ_VISIBLE.ALL)

		selected = new_selected
		_gmeshMenu_about_to_show()

func scene_closed(root):
	#clear()
	pass


func _gmeshMenu_about_to_show():
	var popup = gmeshMenu.get_node("MenuButton").get_popup()
	var editCheckBox = gmeshMenu.get_node("EditCheckBox")
	var editing = true
	var xray = true

	if selected.size() == 0:
		editing = false
		xray = false
	else:
		for mesh_instance in selected:

			if gmeshViz.has_data(mesh_instance) and gmeshEdit.has_data(mesh_instance):
				if not gmeshEdit.is_editing(mesh_instance):
					editing = false
				var viz_data = gmeshViz.get_data(mesh_instance)
				if not viz_data.has_meta("mode") or viz_data.get_meta("mode") == null:
					xray = false
			else:
				editing = false
				xray = false

	editCheckBox.pressed = editing
	popup.set_item_disabled(0,editing == false)
	popup.set_item_checked(0,editing and xray)


func _gmeshMenu_index_pressed(index):
	# Handles menu actions

	var popup = gmeshMenu.get_node("MenuButton").get_popup()

	if index == 0: #X-RAY
		for mesh_instance in selected:
			var viz_data = gmeshViz.get_data(mesh_instance)
			if popup.is_item_checked(index):
				# make not xray
				viz_data.set_meta("mode",null)
			else:
				# make xray
				viz_data.set_meta("mode",gmeshViz.VIZ_VISIBLE.TRIS | gmeshViz.VIZ_VISIBLE.LINES | gmeshViz.VIZ_VISIBLE.POINTS)

			if viz_data.has_meta("mode") and viz_data.get_meta("mode") != null:
				gmeshViz.set_visible(mesh_instance,viz_data.get_meta("mode"))
			else:
				gmeshViz.set_visible(mesh_instance,gmeshViz.VIZ_VISIBLE.ALL)

func _gmeshMenu_edit_toggled(editing):
	if not editing:
		disable_editing()
	else:
		for mesh_instance in selected:
			if not gmeshViz.has_data(mesh_instance):
				gmeshViz.create_viz_instance(mesh_instance)
			if not gmeshEdit.has_data(mesh_instance):
				gmeshEdit.create_edit_data(mesh_instance,gmeshViz.get_gmesh_ref(mesh_instance))
			gmeshEdit.set_editing(mesh_instance,true)

			var viz_data = gmeshViz.get_data(mesh_instance)
			if viz_data.has_meta("mode") and viz_data.get_meta("mode") != null:
				gmeshViz.set_visible(mesh_instance,viz_data.get_meta("mode"))
			else:
				gmeshViz.set_visible(mesh_instance,gmeshViz.VIZ_VISIBLE.ALL)
