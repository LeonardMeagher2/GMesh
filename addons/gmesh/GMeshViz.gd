tool
extends Node

var plugin:EditorPlugin

var state = {}

var GMesh = load("res://addons/gmesh/gmesh.gd")
var outline_material = load("res://addons/gmesh/outline_material.tres")
var wire_faces_material = load("res://addons/gmesh/wire_faces_material.tres")
var vertex_wire_shader = load("res://addons/gmesh/vertex_wire_shader.tres")

var selected_color = Color8(255, 196, 21,255)
var unselected_color = Color8(10,10,10,255)

enum VIZ_VISIBLE {
	NONE = 0,
	TRIS = 1,
	LINES = 2,
	POINTS = 4,
	SOLID = 8,
	ALL = 15
}

class VizData:
	var GMesh = load("res://addons/gmesh/gmesh.gd")
	
	var tri_viz:RID = VisualServer.immediate_create()
	var line_viz:RID = VisualServer.immediate_create()
	var point_viz:RID = VisualServer.immediate_create()
	var solid_viz:RID = VisualServer.immediate_create()
	
	var gmesh_instance = GMesh.new()
	var ref_count:int = 0
	var mesh_ref:WeakRef
	
	func _init(mesh:Mesh):
		mesh_ref = weakref(mesh)
		gmesh_instance.from_mesh(mesh)
		gmesh_instance.add_user_signal("update")
		gmesh_instance.add_user_signal("edit")
		
	func ref():
		ref_count += 1
	func unref():
		ref_count -= 1
	
class VizInstanceData:
	var tri_viz_instance:RID
	var line_viz_instance:RID
	var point_viz_instance:RID
	var solid_viz_instance:RID
	var original_instance:RID
	
	var mesh_ref:WeakRef
	var mesh_key:int
	var last_transform:Transform
	
	var original_visibility:int = VIZ_VISIBLE.NONE
	var mesh_visible:bool
	var visible:int = 0
	
	func _init(mesh_instance:MeshInstance, mesh:Mesh, viz_data:VizData, scenario:RID):
		
		tri_viz_instance = VisualServer.instance_create2(viz_data.tri_viz,scenario)
		line_viz_instance = VisualServer.instance_create2(viz_data.line_viz,scenario)
		point_viz_instance = VisualServer.instance_create2(viz_data.point_viz,scenario)
		solid_viz_instance = VisualServer.instance_create2(viz_data.solid_viz,scenario)
		original_instance = mesh_instance._get_visual_instance_rid()
		
		mesh_visible = mesh_instance.visible
		set_transform(mesh_instance.global_transform)
		set_visible(visible)
		
		self.mesh_ref = weakref(mesh)
		mesh_key = hash(mesh)
		viz_data.ref()
		
	func set_transform(transform:Transform):
		if transform != last_transform:
			VisualServer.instance_set_transform(tri_viz_instance,transform)
			VisualServer.instance_set_transform(line_viz_instance,transform)
			VisualServer.instance_set_transform(point_viz_instance,transform)
			VisualServer.instance_set_transform(solid_viz_instance,transform)
			VisualServer.instance_set_transform(original_instance, transform)
			last_transform = transform
			
	func set_visible(visible:int):
		self.visible = visible
		
		VisualServer.instance_set_visible(tri_viz_instance,mesh_visible and visible & VIZ_VISIBLE.TRIS == VIZ_VISIBLE.TRIS)
		VisualServer.instance_set_visible(line_viz_instance,mesh_visible and visible & VIZ_VISIBLE.LINES == VIZ_VISIBLE.LINES)
		VisualServer.instance_set_visible(point_viz_instance,mesh_visible and visible & VIZ_VISIBLE.POINTS == VIZ_VISIBLE.POINTS)
		VisualServer.instance_set_visible(solid_viz_instance,mesh_visible and visible & VIZ_VISIBLE.SOLID == VIZ_VISIBLE.SOLID)
		VisualServer.instance_set_visible(original_instance, mesh_visible if visible == VIZ_VISIBLE.NONE else false)
		
		pass
	

func create_viz_data(mesh:Mesh) -> VizData:
	
	var viz_data:VizData = set_data(mesh, VizData.new(mesh))
	mesh.connect("changed",self,"mesh_resource_updated",[mesh],CONNECT_REFERENCE_COUNTED)
	viz_data.gmesh_instance.connect("edit",self,"mesh_resource_edited",[viz_data],CONNECT_REFERENCE_COUNTED)
	update_viz_data(mesh)
	
	return viz_data
	
func create_viz_instance(mesh_instance:MeshInstance) -> VizInstanceData:
	var mesh:Mesh = mesh_instance.mesh
	
	var viz_data:VizData
	if has_data(mesh):
		viz_data = get_data(mesh)
	else:
		viz_data = create_viz_data(mesh)
		
	var viz_instance_data:VizInstanceData = set_data(mesh_instance, VizInstanceData.new(mesh_instance,mesh,viz_data,get_tree().root.world.scenario))
	
	if mesh_instance.material_override:
		viz_data.gmesh_instance.material = mesh_instance.material_override
	#mesh_instance.connect("tree_exiting",self,"mesh_instance_removed",[hash(mesh_instance)],CONNECT_REFERENCE_COUNTED)
	mesh_instance.connect("visibility_changed",self,"mesh_instance_visibility_changed",[mesh_instance],CONNECT_REFERENCE_COUNTED)
	
	return viz_instance_data

func get_gmesh_ref(object) -> WeakRef:
	if has_data(object):
		var data = get_data(object)
		if data is VizInstanceData:
			data = get_data(data.mesh_key)
		if data:
			return weakref(data.gmesh_instance)
	return null
	
func has_data(object) -> bool:
	return state.has(hash(object))
	pass
func get_data(object):
	return state.get(hash(object))
	
func set_data(object, data):
	var key:int = hash(object)
	state[key] = data
	return state[key]
	
	
func update_viz_data(mesh:Mesh):
	"""
	TODO:
		- Add bone, and weight data
		-  
	"""
	if not has_data(mesh):
		return
	
	var viz_data:VizData = get_data(mesh)
	var selected_verts = Array()
	var selected_edges = Array()
	var selected_faces = Array()
	if viz_data.gmesh_instance.has_meta("selected_verts"):
		selected_verts = viz_data.gmesh_instance.get_meta("selected_verts") as Array
	if viz_data.gmesh_instance.has_meta("selected_edges"):
		selected_edges = viz_data.gmesh_instance.get_meta("selected_edges") as Array
	if viz_data.gmesh_instance.has_meta("selected_faces"):
		selected_faces = viz_data.gmesh_instance.get_meta("selected_faces") as Array
	
	# Solid mesh
	VisualServer.immediate_clear(viz_data.solid_viz)
	VisualServer.immediate_begin(viz_data.solid_viz,VisualServer.PRIMITIVE_TRIANGLES)
	for loop in viz_data.gmesh_instance.loops:
		VisualServer.immediate_normal(viz_data.solid_viz,loop.normal)
		VisualServer.immediate_tangent(viz_data.solid_viz,loop.tangent)
		VisualServer.immediate_color(viz_data.solid_viz,loop.color)
		VisualServer.immediate_uv(viz_data.solid_viz,loop.uv)
		VisualServer.immediate_uv2(viz_data.solid_viz,loop.uv2)
		VisualServer.immediate_vertex(viz_data.solid_viz,loop.vert.position)
	
	VisualServer.immediate_set_material(viz_data.solid_viz,viz_data.gmesh_instance.material)
	VisualServer.immediate_end(viz_data.solid_viz)
	
	# Triangle mesh
	VisualServer.immediate_clear(viz_data.tri_viz)
	VisualServer.immediate_begin(viz_data.tri_viz,VisualServer.PRIMITIVE_TRIANGLES)
	for loop in viz_data.gmesh_instance.loops:
		if selected_faces.has(loop.face.index):
			VisualServer.immediate_color(viz_data.tri_viz,selected_color)
		else:
			VisualServer.immediate_color(viz_data.tri_viz,unselected_color)
		
		VisualServer.immediate_normal(viz_data.tri_viz,loop.vert.normal)
		VisualServer.immediate_vertex(viz_data.tri_viz,loop.vert.position)
	
	VisualServer.immediate_set_material(viz_data.tri_viz,wire_faces_material.get_rid())
	VisualServer.immediate_end(viz_data.tri_viz)
	
	#Line Mesh
	VisualServer.immediate_clear(viz_data.line_viz)
	VisualServer.immediate_begin(viz_data.line_viz,VisualServer.PRIMITIVE_LINES)
	for edge in viz_data.gmesh_instance.edges:
		var is_selected = selected_edges.has(edge.index)
		if not is_selected:
			for face in edge.faces:
				if selected_faces.has(face.index):
					is_selected = true
					break
		
		for vert in edge.verts:
			if is_selected:
				VisualServer.immediate_color(viz_data.line_viz,selected_color)
			else:
				VisualServer.immediate_color(viz_data.line_viz,unselected_color)
		
			VisualServer.immediate_normal(viz_data.line_viz,vert.normal)
			VisualServer.immediate_vertex(viz_data.line_viz,vert.position)
	
	VisualServer.immediate_set_material(viz_data.line_viz,vertex_wire_shader.get_rid())
	VisualServer.immediate_end(viz_data.line_viz)
	
	#Point Mesh
	VisualServer.immediate_clear(viz_data.point_viz)
	VisualServer.immediate_begin(viz_data.point_viz,VisualServer.PRIMITIVE_POINTS)
	for vert in viz_data.gmesh_instance.verts:
		var is_selected = selected_verts.has(vert.index)
		if not is_selected:
			for face in vert.faces:
				if selected_faces.has(face.index):
					is_selected = true
					break
			if not is_selected:
				for edge in vert.edges:
					if selected_edges.has(edge.index):
						is_selected = true
						break
				
		if is_selected:
			VisualServer.immediate_color(viz_data.point_viz,selected_color)
		else:
			VisualServer.immediate_color(viz_data.point_viz,unselected_color)
		
		VisualServer.immediate_normal(viz_data.point_viz,vert.normal)
		VisualServer.immediate_vertex(viz_data.point_viz,vert.position)
	
	VisualServer.immediate_set_material(viz_data.point_viz,vertex_wire_shader.get_rid())
	VisualServer.immediate_end(viz_data.point_viz)

func update_viz_instance(mesh_instance:MeshInstance):
	if not has_data(mesh_instance):
		return
	var viz_instance_data:VizInstanceData = get_data(mesh_instance)
	
	if not mesh_instance.mesh:
		print("clean: has no mesh")
		clean_viz(hash(mesh_instance))
		return
	
	if viz_instance_data.mesh_ref.get_ref() != mesh_instance.mesh:
		print("clean: mesh_ref changed")
		clean_viz(hash(mesh_instance))
		viz_instance_data = create_viz_instance(mesh_instance) 
	
	viz_instance_data.set_transform(mesh_instance.global_transform)

func set_visible(mesh_instance:MeshInstance, visible:int = VIZ_VISIBLE.ALL):
	if not has_data(mesh_instance):
		return
	var viz_instance_data:VizInstanceData = get_data(mesh_instance)
	
	viz_instance_data.mesh_visible = mesh_instance.visible
	viz_instance_data.original_visibility = visible
	viz_instance_data.set_visible(visible)
	
func _exit_tree():
	for key in state:
		print("clean: exit_tree")
		clean_viz(key)
	state = {}

func clean_viz(key):
	if state[key] is VizData:
		var viz_data:VizData = state[key]
		VisualServer.free_rid(viz_data.tri_viz)
		viz_data.tri_viz = RID()
		VisualServer.free_rid(viz_data.line_viz)
		viz_data.line_viz = RID()
		VisualServer.free_rid(viz_data.point_viz)
		viz_data.point_viz = RID()
		VisualServer.free_rid(viz_data.solid_viz)
		viz_data.solid_viz = RID()
		
	elif state[key] is VizInstanceData:
		var viz_instance_data:VizInstanceData = state[key]
		VisualServer.free_rid(viz_instance_data.tri_viz_instance)
		viz_instance_data.tri_viz_instance = RID()
		VisualServer.free_rid(viz_instance_data.line_viz_instance)
		viz_instance_data.line_viz_instance = RID()
		VisualServer.free_rid(viz_instance_data.point_viz_instance)
		viz_instance_data.point_viz_instance = RID()
		VisualServer.free_rid(viz_instance_data.solid_viz_instance)
		viz_instance_data.solid_viz_instance = RID()
		
		viz_instance_data.original_instance = RID()
		
		if viz_instance_data.mesh_key and state.has(viz_instance_data.mesh_key):
			var viz_data:VizData = state[viz_instance_data.mesh_key]
			viz_data.unref()
			if viz_data.ref_count == 0:
				print("clean: ref_count 0")
				clean_viz(viz_instance_data.mesh_key)
	state.erase(key)
	
func mesh_resource_updated(mesh:Mesh):
	var viz_data:VizData = get_data(mesh)
	viz_data.gmesh_instance.from_mesh(mesh)
	update_viz_data(mesh)
	
func mesh_resource_edited(viz_data:VizData):
	var mesh = viz_data.mesh_ref.get_ref()
	if mesh:
		update_viz_data(mesh)
	
func mesh_instance_removed(key:int):
	print("clean: mesh instance removed")
	clean_viz(key)
	
func mesh_instance_visibility_changed(mesh_instance:MeshInstance):
	var viz_instance_data:VizInstanceData = get_data(mesh_instance)
	viz_instance_data.mesh_visible = mesh_instance.visible
	if mesh_instance.visible:
		viz_instance_data.set_visible(viz_instance_data.original_visibility)
	else:
		viz_instance_data.set_visible(VIZ_VISIBLE.NONE)

func draw_over_viewport(overlay:Control):
	pass

func get_state():
	for key in state:
		if state[key] is VizInstanceData:
			var viz_instance_data:VizInstanceData = state[key]
			viz_instance_data.set_visible(VIZ_VISIBLE.NONE)
	var ret = state
	state = {}
	return ret
	
func set_state(state):
	self.state = state
	for key in state:
		print("get_state: ",key)
		if state[key] is VizInstanceData:
			var viz_instance_data:VizInstanceData = state[key]
			viz_instance_data.set_visible(viz_instance_data.original_visibility)