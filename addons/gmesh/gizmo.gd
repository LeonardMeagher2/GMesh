extends EditorSpatialGizmoPlugin

func get_name():
    return "GMeshGizmo"
	
func has_gizmo(spatial):
    return spatial is MeshInstance
	
func redraw(gizmo):
	print(gizmo)
	gizmo.clear()