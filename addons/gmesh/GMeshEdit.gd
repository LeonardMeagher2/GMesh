tool
extends Node

signal changed

var plugin:EditorPlugin

var undo_redo:UndoRedo = UndoRedo.new()
var state = {}
var currently_editing = []

var cursor = Cursor.new()

class Cursor:
	var region_select:bool = false
	var region_begin:Vector2
	var region_end:Vector2
	
	func select_start(pos:Vector2):
		region_select = true
		region_begin = pos
		region_end = pos
	
	func select_end(pos:Vector2):
		if region_select:
			region_end = pos

class EditData:
	var selected_verts:Array
	var selected_edges:Array
	var selected_faces:Array
	
	var gmesh_ref:WeakRef
	var is_editing:bool = false
	
	func _init(mesh_instance:MeshInstance, gmesh_ref:WeakRef):
		self.gmesh_ref = gmesh_ref
		#var gmesh = gmesh_ref.get_ref()
		update_meta()
	
	func update_meta():
		var gmesh = gmesh_ref.get_ref()
		if gmesh:
			selected_faces = [0]
			gmesh.set_meta("selected_verts",selected_verts)
			gmesh.set_meta("selected_edges",selected_edges)
			gmesh.set_meta("selected_faces",selected_faces)
			gmesh.emit_signal("edit")
			

func create_edit_data(mesh_instance:MeshInstance, gmesh_ref:WeakRef) -> EditData:
	var edit_data = set_data(mesh_instance, EditData.new(mesh_instance, gmesh_ref))
	return edit_data

func set_data(mesh_instance:MeshInstance, edit_data:EditData) -> EditData:
	var key:int = hash(mesh_instance)
	state[key] = edit_data
	return state[key]
	
func has_data(mesh_instance:MeshInstance) -> bool:
	return state.has(hash(mesh_instance))
	
func get_data(mesh_instance:MeshInstance) -> EditData:
	return state.get(hash(mesh_instance))

func set_editing(mesh_instance:MeshInstance,editing:bool = true):
	if not has_data(mesh_instance):
		return
	var edit_data = get_data(mesh_instance)
	edit_data.is_editing = editing
	if editing:
		if currently_editing.has(edit_data) == false:
			currently_editing.append(edit_data)
	else:
		currently_editing.erase(edit_data)
		
func is_editing(mesh_instance:MeshInstance) -> bool:
	if not has_data(mesh_instance):
		return false
	var edit_data = get_data(mesh_instance)
	return edit_data.is_editing

func handle_input(camera:Camera, event:InputEvent) -> bool:
	if currently_editing.size():
		if event is InputEventMouseButton:
			pass
	
	return false
	
func draw_over_viewport(overlay:Control):
	pass
	
func get_state():
	var ret = state
	state = {}
	currently_editing = []
	return ret
	
func set_state(state):
	self.state = state
	currently_editing = []
	for key in state:
		var edit_data = state[key]
		if edit_data.is_editing:
			currently_editing.append(edit_data)