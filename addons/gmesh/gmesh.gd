extends Resource

const UNIT_EQUILATERAL_TRIANGLE_HEIGHT = sqrt(6.0)/4.0 + sqrt(6.0)/12.0

class Vertex:
	# A Vertex in a mesh
	var index:int = -1
	var position : = Vector3()
	var normal : = Vector3()
	var pointiness : = 0.0
	var crease : = 0.0

	var edges : = [] # edges connected to this vertex
	var loops : = [] # loops that use this vertex
	var faces : = [] # faces connected to this vertex

	var mesh:Resource

	var bones : = PoolIntArray()
	var weights : = PoolRealArray()

	var selected : = false

	func _init(position:Vector3 = Vector3(), index:int = -1):
		self.index = index
		self.position = position

	func make_dirty() -> Vertex:
		if is_valid():
			for loop in loops:
				loop.dirty = true
		return self

	func copy_from(other:Vertex) -> Vertex:
		position = other.position
		normal = other.normal

		edges = other.edges.duplicate()
		loops = other.loops.duplicate()
		faces = other.faces.duplicate()
		return self

	func calc_normal() -> Vertex:
		normal = Vector3()
		if faces.size() > 0:
			for face in faces:
				normal += face.normal
			normal = normal.normalized()
		
		return self

	func remove() -> Vertex:
		index = -1
		for edge in edges:
			edge.remove()
		for loop in loops:
			loop.remove()
		for face in faces:
			face.remove()
		return self

	func is_valid() -> bool:
		return mesh != null

	func set_smooth(smooth:bool) -> Vertex:
		for loop in loops:
			loop.set_smooth(smooth)
		return self

	func calc_pointiness() -> float:
		var p : = 0.0
		var fPI : = 1.0/PI
		if loops.size() > 0:
			var n : = Vector3()
			for loop in loops:
				n += (loop.next.vert.position - loop.vert.position).normalized()
			p = (acos(clamp(normal.dot(n/(loops.size())),-1.0,1.0)) * fPI)
		self.pointiness = p
		return p

	func calc_crease() -> float:
		var c : = 0.0
		var fPI : = 1.0/PI
		if edges.size() > 0:
			for edge in edges:
				c += acos(edge.get_face_angle()) * fPI
			c /= edges.size()
		self.crease = c
		return c

class Edge:
	# An edge connecting to verticies
	var index:int= -1
	var smooth : = false

	var verts : = [] # the verticies this edge uses (always 2)
	var loops : = [] # the loops this edge connects
	var faces : = [] # faces connected to this edge (0-2)

	var mesh:Resource

	var selected : = false

	func _init(verts:Array, index:int = -1):
		self.index = index
		self.verts = verts.duplicate()


	func make_dirty() -> Edge:
		if is_valid():
			for loop in loops:
				loop.dirty = true
		return self

	func copy_from(other:Edge) -> Edge:
		smooth = other.smooth

		verts = other.verts.duplicate()
		loops = other.loops.duplicate()
		faces = other.faces.duplicate()
		return self

	func get_face_angle() -> float:
		if faces.size() == 2:
			return clamp(faces[0].normal.normalized().dot(faces[1].normal.normalized()),-1.0,1.0)
		return 1.0

	func length() -> float:
		return verts[0].position.distance_to(verts[1].position)
	func length_squared() -> float:
		return verts[0].position.distance_squared_to(verts[1].position)

	func remove() -> Edge:
		index = -1
		verts[0].edges.erase(self)
		verts[1].edges.erase(self)
		for loop in loops:
			loop.edge = null
		for face in faces:
			face.edges.erase(self)
		verts.clear()
		loops.clear()
		faces.clear()

		mesh.edges.erase(self)
		mesh = null

		return self

	func is_valid() -> bool:
		return mesh != null and verts.size() == 2

	func set_smooth(smooth:bool) -> Edge:
		for loop in loops:
			loop.set_smooth(smooth)
		return self

	func get_normal() -> Vector3:
		return (verts[1].position - verts[0].position).normalized()



class Loop:
	# per face vertex data, and a corner of a face
	var dirty : = true setget set_dirty,get_dirty
	var index:int = -1
	var edge:Edge
	var vert:Vertex
	var face:Face
	var mesh:Resource

	var next:Loop
	var prev:Loop

	var color : = Color()
	var tangent : = Plane()
	var uv : = Vector2()
	var uv2 : = Vector2()
	var normal : = Vector3() # this is the loop's normal

	var smooth : = false

	func _init(vert, index = -1):
		self.index = index
		self.vert = vert

	func set_dirty(d:bool) -> Loop:
		if is_valid():
			if dirty == false and d == true:
				pass #mesh.update_mesh_data(self)

			dirty = d
		else:
			dirty = true
		return self

	func get_dirty() -> bool:
		return dirty


	func copy_from(other:Loop) -> Loop:
		self.edge = other.edge
		self.vert = other.vert
		self.face = other.face
		self.next = other.next
		self.prev = other.prev

		self.color = other.color
		self.tangent = other.tangent
		self.uv = other.uv
		self.uv2 = other.uv2
		self.normal = other.normal
		self.smooth = other.smooth

		return self

	func remove() -> Loop:
		index = -1
		vert.loops.erase(self)
		vert = null
		edge.loops.erase(self)
		edge = null
		face.loops.erase(self)
		face = null
		if next:
			next.prev = null
			next = null
		if prev:
			prev.next = null
			prev = null

		mesh.loops.erase(self)
		mesh = null
		return self

	func is_valid() -> bool:
		return mesh != null and vert != null and edge != null and face != null and next != null and prev != null

	func flip() -> Loop:
		var n:Loop = next
		next = prev
		prev = n
		return self

	func set_smooth(smooth:bool) -> Loop:
		self.smooth = smooth
		calc_normal()
		return self

	func calc_normal() -> Vector3:
		normal = Vector3()
		if is_valid():
			if smooth:
				normal = vert.normal
			else:
				normal = face.normal
		return normal

class Face:
	# face with 3 loops
	var index:int = -1
	var normal : = Vector3()
	var area : = 0.0

	var verts : = [] # verts of this face
	var edges : = [] # edges of this face
	var loops : = [] # loops of this face

	var mesh:Resource

	var selected : = false

	func _init(loops:Array, index:int = -1):
		self.index = index
		self.loops = loops

	func make_dirty() -> Face:
		if is_valid():
			for loop in loops:
				loop.dirty = true
		return self


	func copy_from(other:Face) -> Face:
		self.normal = other.normal

		self.verts = other.verts.duplicate()
		self.edges = other.edges.duplicate()
		self.loops = other.loops.duplicate()
		return self

	func remove() -> Face:
		
		loops[0].remove()
		loops[0].remove()
		loops[0].remove()

		verts.clear()
		edges.clear()
		loops.clear()

		mesh.faces.erase(self)
		mesh = null
		return self

	func calc_normal() -> Vector3:
		normal = Vector3()
		if is_valid():
			normal = (-(verts[1].position - verts[0].position).cross(verts[2].position - verts[0].position)).normalized()
		return normal

	func flip() -> Face:
		if is_valid():
			loops[0].flip()
			loops[1].flip()
			loops[2].flip()

			loops.invert()
			verts.invert()
		return self

	func is_valid() -> bool:
		return mesh != null and loops.size() == 3  and verts.size() == 3 and edges.size() == 3

	func set_smooth(smooth) -> Face:
		if is_valid():
			loops[0].set_smooth(smooth)
			loops[1].set_smooth(smooth)
			loops[2].set_smooth(smooth)
		return self

	func get_center() -> Vector3:
		return (verts[0].position + verts[1].position + verts[2].position) / 3.0;

	func vector_to_uv(v:Vector3) -> Vector3:
		var bc:Vector3 = get_barycentric(v,loops[0].vert.position, loops[1].vert.position, loops[2].vert.position)
		return (loops[0].uv * bc.x) + (loops[1].uv * bc.y) + (loops[2].uv * bc.z);

	func uv_to_vector(uv:Vector2) -> Vector3:
		var bc:Vector3 = get_barycentric(Vector3(uv.x,uv.y,0),Vector3(loops[0].uv.x,loops[0].ux.y,0), Vector3(loops[1].uv.x,loops[1].ux.y,0), Vector3(loops[2].uv.x,loops[2].ux.y,0))
		return (loops[0].vert.position * bc.x) + (loops[1].vert.position * bc.y) + (loops[2].vert.position * bc.z);

	func is_backfacing(normal:Vector3) -> bool:
		return self.normal.dot(normal) < 0.0

	func calc_area() -> float:
		var ab:Vector3 = loops[1].vert.position - loops[0].vert.position
		var ac:Vector3 = loops[2].vert.position - loops[0].vert.position
		self.area = ab.cross(ac).length() * 0.5
		return self.area
		
	func poke(offset:float = 0.0) -> Array:
		var v:Vertex = mesh.add_vertex(get_center() + calc_normal() * offset)
		var new_faces : = [
			mesh.add_face(verts[0],verts[1],v),
			mesh.add_face(verts[1],verts[2],v),
			mesh.add_face(verts[0],v,verts[2])
		]
		remove()
		return new_faces
		
	func ray_intersects(from:Vector3, dir:Vector3):
		return Geometry.ray_intersects_triangle(from,dir,verts[0].position,verts[1].position,verts[2].position)
		
	static func get_barycentric(v:Vector3, a:Vector3, b:Vector3, c:Vector3) -> Vector3:
		var mat1 : = Basis(a, b, c)
		var det : = mat1.determinant()
		var mat2 : = Basis(v, b, c)
		var factor_alpha : = mat2.determinant()
		var mat3 : = Basis(v, c, a)
		var factor_beta : = mat3.determinant()
		var alpha : = factor_alpha / det
		var beta : = factor_beta / det
		var gamma : = 1.0 - alpha - beta
		return Vector3(alpha, beta, gamma)
		
	
		

""" 
Start of gmesh 
	TODO:
		- add from_arrays() method and figure out how indexed arrays work.

"""


var verts : = [] # all the verts in this mesh
var edges : = [] # all the edges in this mesh
var loops : = [] # all the loops in this mesh
var faces : = [] # all the faces in this mesh
var material:Material
var primitive_type : = Mesh.PRIMITIVE_TRIANGLES

# WARN: Exporting dictionaries with a type may cause them to be shared across instances?
var _edge_hash = {}
var _face_hash = {}

func _init():
	pass

func make_dirty() -> Resource:
	for loop in loops:
		loop.dirty = true
	return self

func clear() -> Resource:
	_edge_hash.clear()
	_face_hash.clear()

	verts.clear()
	edges.clear()
	loops.clear()
	faces.clear()
	
	return self

func set_smooth(smooth:bool) -> Resource:
	for loop in loops:
		loop.set_smooth(smooth)
	return self

func add_vertex(v:Vector3) -> Vertex:
	var vert:Vertex = Vertex.new(v,verts.size())
	vert.mesh = self
	verts.append(vert)
	return vert

func add_edge(v1:Vertex,v2:Vertex) -> Edge:

	var key : = [v1,v2]
	key.sort_custom(self,"_sort_by_index")

	if _edge_hash.has(key) == false:
		var edge:Edge = Edge.new([v1,v2],edges.size())
		edge.mesh = self
		edges.append(edge)
		v1.edges.append(edge)
		v2.edges.append(edge)
		_edge_hash[key] = edge

	return _edge_hash[key]

func has_edge(v1:Vertex,v2:Vertex) -> bool:
	assert(v1 is Vertex)
	assert(v2 is Vertex)

	var key : = [v1,v2]
	key.sort_custom(self,"_sort_by_index")

	return _edge_hash.has(key)

func add_loop(v:Vertex) -> Loop:
	var l:Loop = Loop.new(v,loops.size())
	v.loops.append(l)
	l.mesh = self
	loops.append(l)

	return l

func add_face(v1:Vertex,v2:Vertex,v3:Vertex) -> Face:

	var key : = [v1,v2,v3]
	key.sort_custom(self,"_sort_by_index")

	if _face_hash.has(key) == false:

		#this will check the edge hash
		var e1:Edge = add_edge(v1,v2)
		var e2:Edge = add_edge(v2,v3)
		var e3:Edge = add_edge(v3,v1)
		

		#loops are always unique to the face
		var l1:Loop = add_loop(v1)
		var l2:Loop = add_loop(v2)
		var l3:Loop = add_loop(v3)

		var f:Face = Face.new([l1,l2,l3],faces.size())
		

		l1.next = l2
		l2.next = l3
		l3.next = l1
		l3.prev = l2
		l2.prev = l1
		l1.prev = l3

		l1.edge = e1
		l2.edge = e2
		l3.edge = e3
		e1.loops.append(l1)
		e2.loops.append(l2)
		e3.loops.append(l3)

		v1.faces.append(f)
		v2.faces.append(f)
		v3.faces.append(f)

		e1.faces.append(f)
		e2.faces.append(f)
		e3.faces.append(f)

		l1.face = f
		l2.face = f
		l3.face = f

		f.verts.append(v1)
		f.verts.append(v2)
		f.verts.append(v3)

		f.edges.append(e1)
		f.edges.append(e2)
		f.edges.append(e3)

		f.mesh = self
		faces.append(f)
		_face_hash[key] = f

	return _face_hash[key]
	
func add_quad(v1:Vertex,v2:Vertex,v3:Vertex,v4:Vertex) -> Array:
	return [add_face(v1,v2,v3),add_face(v2,v4,v1)]

func from_mesh(mesh:Mesh,surface:int = 0,auto_merge:bool = false) -> Resource:
	
	material = mesh.material
	
	if mesh is PrimitiveMesh:
		var arr:Array = mesh.get_mesh_arrays()
		mesh = ArrayMesh.new()
		auto_merge = true
		mesh.add_surface_from_arrays(Mesh.PRIMITIVE_TRIANGLES,arr)
		surface = mesh.get_surface_count()-1

	clear()
	
	var _data : = MeshDataTool.new()
	_data.create_from_surface(mesh,surface)
	primitive_type = (mesh as ArrayMesh).surface_get_primitive_type(surface)

	var _vert_hash : = {}

	for i in range(_data.get_face_count()):

		var v1i:int = _data.get_face_vertex(i,0)
		var v2i:int = _data.get_face_vertex(i,1)
		var v3i:int = _data.get_face_vertex(i,2)

		var v1p:Vector3 = _data.get_vertex(v1i)
		var v2p:Vector3 = _data.get_vertex(v2i)
		var v3p:Vector3 = _data.get_vertex(v3i)

		var v1:Vertex
		var v2:Vertex
		var v3:Vertex
		if auto_merge:
			if _vert_hash.has(v1p):
				v1 = _vert_hash[v1p]
			else:
				v1 = add_vertex(v1p)
				_vert_hash[v1p] = v1

			if _vert_hash.has(v2p):
				v2 = _vert_hash[v2p]
			else:
				v2 = add_vertex(v2p)
				_vert_hash[v2p] = v2

			if _vert_hash.has(v3p):
				v3 = _vert_hash[v3p]
			else:
				v3 = add_vertex(v3p)
				_vert_hash[v3p] = v3
		else:
			v1 = add_vertex(v1p)
			v2 = add_vertex(v2p)
			v3 = add_vertex(v3p)

		if v1.index == v2.index or v1.index == v3.index or v2.index == v3.index:
			continue


		var face:Face = add_face(v1,v2,v3)

		face.normal = _data.get_face_normal(i)
		var l1:Loop = face.loops[0]
		var l2:Loop = face.loops[1]
		var l3:Loop = face.loops[2]

		v1.bones = _data.get_vertex_bones(v1i)
		v2.bones = _data.get_vertex_bones(v2i)
		v3.bones = _data.get_vertex_bones(v3i)

		v1.weights = _data.get_vertex_weights(v1i)
		v2.weights = _data.get_vertex_weights(v2i)
		v3.weights = _data.get_vertex_weights(v3i)

		l1.color = _data.get_vertex_color(v1i)
		l2.color = _data.get_vertex_color(v2i)
		l3.color = _data.get_vertex_color(v3i)

		l1.normal = _data.get_vertex_normal(v1i)
		l2.normal = _data.get_vertex_normal(v2i)
		l3.normal = _data.get_vertex_normal(v3i)

		l1.tangent = _data.get_vertex_tangent(v1i)
		l2.tangent = _data.get_vertex_tangent(v2i)
		l3.tangent = _data.get_vertex_tangent(v3i)

		l1.uv = _data.get_vertex_uv(v1i)
		l2.uv = _data.get_vertex_uv(v2i)
		l3.uv = _data.get_vertex_uv(v3i)

		l1.uv2 = _data.get_vertex_uv2(v1i)
		l2.uv2 = _data.get_vertex_uv2(v2i)
		l3.uv2 = _data.get_vertex_uv2(v3i)

		l1.smooth = l1.normal != face.normal
		l2.smooth = l2.normal != face.normal
		l3.smooth = l3.normal != face.normal

		face.calc_area()
	
	for vert in verts:
		vert.calc_normal()
		vert.calc_pointiness()
		vert.calc_crease()
	
	return self

func get_triangles_arrays():
	var arrays = Array()
	arrays.resize(VisualServer.ARRAY_MAX)
	var verts_array = PoolVector3Array()
	var normals_array = PoolVector3Array()
	var tangents_array = PoolRealArray()
	var colors_array = PoolColorArray()
	var uvs_array = PoolVector2Array()
	var uv2s_array = PoolVector2Array()
	var bones_array = PoolIntArray()
	var weights_array = PoolRealArray()
	
	for loop in loops:
		verts_array.append(loop.vert.position)
		normals_array.append(loop.normal)
		tangents_array.append_array([loop.tangent.x,loop.tangent.y,loop.tangent.z,loop.tangent.d])
		colors_array.append(loop.color)
		uvs_array.append(loop.uv)
		uv2s_array.append(loop.uv2)
		bones_array.append_array(loop.vert.bones)
		weights_array.append_array(loop.vert.weights)
		
	arrays[VisualServer.ARRAY_VERTEX] = verts_array
	arrays[VisualServer.ARRAY_NORMAL] = normals_array
	arrays[VisualServer.ARRAY_TANGENT] = tangents_array
	arrays[VisualServer.ARRAY_COLOR] = colors_array
	arrays[VisualServer.ARRAY_TEX_UV] = uvs_array
	arrays[VisualServer.ARRAY_TEX_UV2] = uv2s_array
	if bones_array.size():
		arrays[VisualServer.ARRAY_BONES] = bones_array
	if weights_array.size():
		arrays[VisualServer.ARRAY_WEIGHTS] = weights_array
		
	return arrays
	
func get_lines_arrays():
	var arrays = Array()
	arrays.resize(VisualServer.ARRAY_MAX)
	var verts_array = PoolVector3Array()
	var normals_array = PoolVector3Array()
	var bones_array = PoolIntArray()
	var weights_array = PoolRealArray()
	
	for edge in edges:
		verts_array.append(edge.verts[0].position)
		verts_array.append(edge.verts[1].position)
		
		normals_array.append(edge.verts[0].normal)
		normals_array.append(edge.verts[1].normal)
		
		bones_array.append_array(edge.verts[0].bones)
		bones_array.append_array(edge.verts[1].bones)
		
		weights_array.append_array(edge.verts[0].weights)
		weights_array.append_array(edge.verts[1].weights)
		
	arrays[VisualServer.ARRAY_VERTEX] = verts_array
	arrays[VisualServer.ARRAY_NORMAL] = normals_array
	if bones_array.size():
		arrays[VisualServer.ARRAY_BONES] = bones_array
	if weights_array.size():
		arrays[VisualServer.ARRAY_WEIGHTS] = weights_array
	
	return arrays
	

func flip() -> Resource:
	for face in faces:
		face.flip()
	return self

func calc_normals() -> Resource:
	for face in faces:
		face.calc_normal()
	for vert in verts:
		vert.calc_normal()

	for loop in loops:
		loop.calc_normal()
	return self

func grow(amount:float = 1.0) -> Resource:
	for vert in verts:
		vert.position += vert.normal * amount
	return self
		
func translate(offset:Vector3) -> Resource:
	for vert in verts:
		vert.position += offset
	return self

func _sort_by_index(a:Vertex,b:Vertex) -> bool:
	return a.index < b.index
	
func get_center() -> Vector3:
	var center : = Vector3()
	for vert in verts:
		center += vert.position
	return center / verts.size()
	
func center_geometry() -> Resource:
	var center = get_center()
	translate(-center)
	return self

func add_pyramid_tetrahedron(radius:float = 1.0, transform:Transform = Transform()) -> Array:
	var sqr3 : = sqrt(3.0)
	var sqr6 : = sqrt(6.0)
	
	var v1 : = add_vertex(transform.xform(Vector3( sqr3/3.0,   0,-sqr6/12.0) * radius))
	var v2 : = add_vertex(transform.xform(Vector3(-sqr3/6.0, 0.5,-sqr6/12.0) * radius))
	var v3 : = add_vertex(transform.xform(Vector3(        0,   0, sqr6/4.0 ) * radius))
	var v4 : = add_vertex(transform.xform(Vector3(-sqr3/6.0,-0.5,-sqr6/12.0) * radius))
	
	add_face(v2,v1,v3) # bottom
	add_face(v3,v4,v2) # left
	add_face(v2,v4,v1) # right front
	add_face(v1,v4,v3) # right back
	
	return [v1,v2,v3,v4]
	
func add_cube_tetrahedron(radius:float = 1.0, transform:Transform = Transform()) -> Array:
	var sqr2 : = sqrt(2.0)/4.0
	
	var v1 : = add_vertex(transform.xform(Vector3(-sqr2,-sqr2,-sqr2) * radius))
	var v2 : = add_vertex(transform.xform(Vector3( sqr2,-sqr2, sqr2) * radius))
	var v3 : = add_vertex(transform.xform(Vector3(-sqr2, sqr2, sqr2) * radius))
	var v4 : = add_vertex(transform.xform(Vector3( sqr2, sqr2,-sqr2) * radius))
	
	add_face(v2,v1,v3) # bottom
	add_face(v3,v4,v2) # left
	add_face(v2,v4,v1) # right front
	add_face(v1,v4,v3) # right back
	
	return [v1,v2,v3,v4]
	
